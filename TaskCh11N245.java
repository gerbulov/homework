import java.util.Arrays;

public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] a = {1, 3, -1, 10, -3, 13, 2, 3, -14};
        int[] b = new int[a.length];
        System.arraycopy(a, 0, b, 0, a.length);
        Arrays.sort(b);
        System.out.println(Arrays.toString(b));
    }
}
