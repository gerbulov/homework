import java.util.Scanner;

public class TaskCh02N013 {
    static void getreverse(int n) {
        if (n < 200 & n > 100) {
            int m = 0;
            for (int i=1; i<=3; i++) {
                m *= 10;
                m += n % 10;
                n /= 10;
            }
            System.out.println("Перевернутое число: " + m);
        } else {
            System.out.println("Введено неправильное число");
        }
    }

    public static void main(String[] args) {
        getreverse(134);
    }
}
