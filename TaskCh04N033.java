public class TaskCh04N033 {
    //Дано натуральное число
    public static String evenorodd(int n) {
        switch (n % 2) {
            case 0:
                return "Число четное";
            default:
                return "Число нечетное";
        }

    }

    public static void main(String[] args) {
        System.out.println(evenorodd(117));
        System.out.println(evenorodd(110));
    }
}
