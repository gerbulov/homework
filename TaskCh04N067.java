import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите номер дня");
        int day = sc.nextInt();

        if (day >= 1 & day <= 365) {
            switch (day % 7) {
                case 6:
                case 0:
                    System.out.println("Weekend");
                    break;
                default:
                    System.out.println("Workday");
            }
        } else System.out.println("Введен неверный день");
    }
}
