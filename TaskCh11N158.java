import java.lang.reflect.Array;
import java.util.Arrays;

public class TaskCh11N158 {
    static int[] delcopy(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int person = arr[i];
            int j = i + 1;
            while (j < arr.length) {
                if (person == arr[j]) {
                    System.arraycopy(arr, j + 1, arr, j, arr.length - 1 - j);
                    arr[arr.length - 1] = 0;
                }
                j++;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 1, 2, 1, 3, 4, 5};
        int[] resultarr = delcopy(arr);
        System.out.println(Arrays.toString(resultarr));
    }
}
