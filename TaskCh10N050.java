public class TaskCh10N050 {
    static int Akkerman (int n, int m) {
        if (n==0) {
            return m+1;
        } else if (n > 0 & m==0) {
            return Akkerman(n-1, 1);
        } else {
            return Akkerman(n-1,Akkerman(n,m-1));
        }
    }

    public static void main(String[] args) {
        System.out.println(Akkerman(3,5));
    }
}
