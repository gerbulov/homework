import java.util.Scanner;

public class TaskCh06N087 {
    static class Game {
        private String team1, team2;
        private int team1score = 0, team2score = 0;
        Scanner sc = new Scanner(System.in);

        // here will be implemented console input logic for 2 players

        void play() {
            System.out.println("Enter team #1:");
            team1 = sc.nextLine();
            System.out.println("Enter team #2:");
            team2 = sc.nextLine();
            teamtoscore();
        }

        private void teamtoscore() {
            System.out.println("Enter team to score (1 or 2 or 0 to finish)");
            int teamforscore = sc.nextInt();
            switch (teamforscore) {
                case 0:
                    System.out.println(result());
                    break;
                case 1:
                    countplus(1);
                    break;
                case 2:
                    countplus(2);
                    break;
            }
        }

        private void countplus(int i) {
            System.out.println("Enter score (1, 2 or 3) for team #" + i);
            int scorethis = sc.nextInt();
            if (scorethis > 3 || scorethis < 1) {
                System.out.println("Error!");
                return;
            }
            if (i == 1) {
                team1score += scorethis;
            } else team2score += scorethis;
            System.out.println(score());
            teamtoscore();
        }

        // intermediate score
        String score() {
            return "Team #1: " + team1score + "\t| Team #2: " + team2score;
        }

        // result with winner, loser and their scores
        String result() {
            if (team1score > team2score) {
                return "Team " + team1 + " WIN!!! Team #1: " + team1score + "\t| Team #2: " + team2score;
            } else return "Team " + team2 + " WIN!!! Team #1: " + team1score + "\t| Team #2: " + team2score;
        }
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

