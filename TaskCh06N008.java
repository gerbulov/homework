import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        int i = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число n");
        int n = sc.nextInt();
        while (i * i <= n) {
            System.out.print(i * i + " ");
            i++;
        }
    }
}
