import java.util.Scanner;

public class TaskCh10N055 {
    static StringBuilder convert(int n, int nsystem, StringBuilder result) {
        final char[] symbols = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int delenie, ostatok;
        if (n < nsystem) result.append(symbols[n]);
        else {
            delenie = n / nsystem;
            ostatok = n % nsystem;
            if (delenie >= nsystem) {
                convert(delenie, nsystem, result);
            } else {
                result.append(symbols[delenie]);
            }
            result.append(symbols[ostatok]);
        }
        return result;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число в 10-ричной системе:");
        int n = sc.nextInt();
        System.out.println("Введите систему счисления от 2 до 16:");
        int nsystem = sc.nextInt();
        StringBuilder result = new StringBuilder();
        System.out.println(convert(n, nsystem, result));
    }
}
