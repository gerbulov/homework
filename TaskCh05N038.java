public class TaskCh05N038 {
    public static void main(String[] args) {
        float rasst=0;
        float summa=0;

        for (float i=1; i<=100; i++) {
            summa = summa + 1/i;
            if (i%2==0) rasst -= 1 / i;
            else rasst += 1/i;

        }
        System.out.println("Общий путь: " + summa +" км; Сейчас на: " + rasst + " км от дома");
    }
}
