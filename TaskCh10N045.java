public class TaskCh10N045 {
    static int nprogress(int first, int step, int nfinish) {
        if (nfinish == 1) return first;
        return nprogress(first + step, step, nfinish - 1);
    }

    static int summnprogress(int first, int step, int nfinish) {
        if (nfinish == 0) return 0;
        int sum = first + step * (nfinish-1);
        return sum + summnprogress(first, step, nfinish - 1);
    }

    public static void main(String[] args) {
        System.out.println(nprogress(1, 2, 4));
        System.out.println(summnprogress(1, 2, 4));
    }
}
