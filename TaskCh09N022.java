import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = sc.nextLine();
        if (word.length()%2==0) System.out.println(word.substring(0, word.length()/2));
        else System.out.println("Задано слово с нечётным количеством букв");

    }
}
