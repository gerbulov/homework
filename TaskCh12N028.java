import java.util.Arrays;

public class TaskCh12N028 {
    static int[][] ulitka(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int l = 0; l <= a / 2; l++) {
            for (int j = l; j < b - l; j++, count++) {
                arr[l][j] = count;
            }

            for (int i = l + 1; i < a - 1 - l; i++, count++) {
                arr[i][b - 1 - l] = count;
            }

            if (l < a / 2) {
                for (int j = b - 1 - l; j >= l; --j, count++) {
                    arr[a - 1 - l][j] = count;
                }

                for (int i = a - 2 - l; i >= l + 1; --i, count++) {
                    arr[i][l] = count;
                }
            }
        }
        return arr;
    }

    static void print(int[][] arr) {
        System.out.println(Arrays.deepToString(arr));
    }

    public static void main(String[] args) {
        print(ulitka(5, 5));
    }
}
