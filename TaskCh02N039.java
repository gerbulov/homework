import java.util.Scanner;

public class TaskCh02N039 {
    private static float degrees(int h, int m, int s) {
        float deg;
        if (h >= 12) h -= 12;
        final float full_seconds = 60 * 60 * 12; //количество секунд в 12 часах
        System.out.println(full_seconds);
        final float seconds = h * 60 * 60 + m * 60 + s; //текущее количество секунд
        System.out.println(seconds);
        System.out.println(deg = seconds / full_seconds);
        return deg = seconds / full_seconds * 360;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество часов: ");
        int h = sc.nextInt();
        if (h < 1 || h > 24) { //по условиям задачи часы должны быть больше 0
            System.out.println("Вы ввели неверное время в часах");
            return;
        }
        System.out.print("Введите количество минут: ");
        int m = sc.nextInt();
        if (m < 0 || m >= 59) {
            System.out.println("Вы ввели неверное время в минутах");
            return;
        }
        System.out.print("Введите количество секунд: ");
        int s = sc.nextInt();
        if (s < 0 || s >= 59) {
            System.out.println("Вы ввели неверное время в секундах");
            return;
        }

        System.out.println("Результат: " + degrees(h, m, s) + " градусов");
    }
}
