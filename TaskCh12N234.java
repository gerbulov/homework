import java.util.Arrays;

public class TaskCh12N234 {
    static int[][] deletecolumn(int c, int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = c - 1; j < arr[i].length - 1; j++) {
                arr[i][j] = arr[i][j + 1];
            }
            arr[i][arr[i].length - 1] = 0;
        }
        return arr;
    }

    static int[][] deletestring(int s, int[][] arr) {
        for (int i = s - 1; i < arr.length-1; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = arr[i+1][j];
                arr[arr[i].length - 1][j] = 0;
            }
        }
        return arr;
    }

    static void result(int[][] result) {
        System.out.println(Arrays.deepToString(result));
    }

    public static void main(String[] args) {
        int[][] arr = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 8},
                {7, 6, 5, 4, 3},
                {2, 1, 2, 3, 4},
                {5, 6, 7, 8, 9}
        };
        result(deletestring(4, arr));
        int[][] arr2 = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 8},
                {7, 6, 5, 4, 3},
                {2, 1, 2, 3, 4},
                {5, 6, 7, 8, 9}
        };
        result(deletecolumn(4, arr2));
    }
}
