public class TaskCh10N046 {
    static int nprogress(int first, int step, int nfinish) {
        if (nfinish == 1) return first;
        return nprogress(first * step, step, nfinish - 1);
    }

    static int summnprogress(int first, int step, int nfinish) {
        if (nfinish == 0) return 0;
        int sum = first;
        for (int i=0; i<nfinish-1; i++) {
            sum *= step;
        }
        return sum + summnprogress(first, step, nfinish - 1);
    }

    public static void main(String[] args) {
        System.out.println(nprogress(5, 3, 4));
        System.out.println(summnprogress(5, 3, 5));
    }
}
