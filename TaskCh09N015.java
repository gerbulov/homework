import java.util.Scanner;

public class TaskCh09N015 {
    //9.15. Дано слово. Вывести на экран его k-й символ
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Напишите слово");
        String letter = sc.nextLine();
        System.out.println("Какую символ по счету вывести, где 0 - это первый символ");
        int k = sc.nextInt();
        System.out.println(letter.charAt(k));
    }
}
