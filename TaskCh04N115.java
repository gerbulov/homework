import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        String animal, color;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите год:");
        int year = sc.nextInt();
        if (year < 1984) {
            System.out.println("Введите год от 1984");
            return;
        }
        int i = (year - 1984) % 60;
        switch (i % 12) {
            case 0:
                animal = "Крыса";
                break;
            case 1:
                animal = "Корова";
                break;
            case 2:
                animal = "Тигр";
                break;
            case 3:
                animal = "Заяц";
                break;
            case 4:
                animal = "Дракон";
                break;
            case 5:
                animal = "Змея";
                break;
            case 6:
                animal = "Лошадь";
                break;
            case 7:
                animal = "Овца";
                break;
            case 8:
                animal = "Обезьяна";
                break;
            case 9:
                animal = "Петух";
                break;
            case 10:
                animal = "Собака";
                break;
            default:
                animal = "Свинья";
                break;
        }

        switch (i % 10) {
            case 0:
            case 1:
                color = "Зелёный";
                break;
            case 2:
            case 3:
                color = "Красный";
                break;
            case 4:
            case 5:
                color = "Жёлтый";
                break;
            case 6:
            case 7:
                color = "Белый";
                break;
            default:
                color = "Черный";
                break;
        }
        System.out.println(year + " - это год " + animal + " " + color);
    }
}
