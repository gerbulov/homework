import java.util.Scanner;

public class TaskCh02N031 {
    private static int findnumber(int n) {
        return (n - n % 100) + (n % 100 % 10 * 10) + ((n - n % 10) / 10 % 10);
    }

    public static void main(String[] args) {
        int n;
        System.out.print("Введите любое целое число от 100 до 999: ");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        if (number >= 100 & number <= 999) {
            n = findnumber(number);
            System.out.println("Правильное число: " + n);
        } else {
            System.out.println("Вы не выполнили условие ввода!");
        }

    }
}