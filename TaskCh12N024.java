import java.util.Arrays;

public class TaskCh12N024 {
    static int[][] array1(int a, int b) {
        int[][] arr = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (i == 0 || j == 0) arr[i][j] = 1;
                else arr[i][j] = arr[i - 1][j] + arr[i][j - 1];
            }
        }
        return arr;
    }

    static int[][] array2(int a, int b) {
        int[][] arr = new int[a][b];
        for (int i = 0; i < a; i++) {
            int count = i;
            for (int j = 0; j < b; j++) {
                count++;
                if (count > 6) {
                    count = 1;
                }
                arr[i][j] = count;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(array1(6, 6)));
        System.out.println(Arrays.deepToString(array2(6, 6)));
    }
}
