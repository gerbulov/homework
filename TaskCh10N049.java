public class TaskCh10N049 {
    static int maxnum(int arr[], int aln) {
        if (aln < 0) return 0;
        int result = aln;
        if (arr[aln] < arr[maxnum(arr, aln - 1)]) {
            result = maxnum(arr, aln - 1);
        }
        return result;
    }

    public static void main(String[] args) {
        int arr[] = {4, 10, 12, 1, 6, 7, 3, 2};
        System.out.println(maxnum(arr, arr.length - 1));
    }
}
