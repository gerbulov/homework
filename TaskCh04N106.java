import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите номер месяца");
        int m = sc.nextInt();
        String season;
        switch (m) {
            case 12:
            case 1:
            case 2:
                season = "зиме";
                break;
            case 3:
            case 4:
            case 5:
                season = "весне";
                break;
            case 6:
            case 7:
            case 8:
                season = "лету";
                break;
            case 9:
            case 10:
            case 11:
                season = "осени";
                break;
            default:
                season = "несуществующему месяцу";
        }
        System.out.println(m + " месяц относится к " + season);
    }
}
