import java.util.ArrayList;

public class coolJavaProjects {
    interface Peoples { // Интерфейс является абстрактным классом
        void hello();

        default void desribeExperience() {
            System.out.println("This is not for u bro! I'm already work =)");
        }
    }

    static class Employer implements Peoples { //инкапсуляция
        @Override
        public void hello() { // полиморфизм
            System.out.println("hi! introduce yourself and describe your java experience please");
        }

    }

    static class Candidate implements Peoples { // наследование класс Candidate наследует интерфейс Peoples
        private String name;   // инкапсуляция переменной внутри класса, аналогично по другим
        private String experience;

        Candidate(String name, String experience) {
            setName(name);
            setExperience(experience);
        }

        private void setName(String name) {
            this.name = name;
        }

        private String getName() {
            return name;
        }

        private void setExperience(String experience) {
            this.experience = experience;
        }

        private String getExperience() {
            return experience;
        }

        @Override
        public void hello() { // полиморфизм
            System.out.println("hi! my name is" + this.name + "!");
        }

        @Override
        public void desribeExperience() { // полиморфизм
            if (this.experience.equals("self-learner")) {
                System.out.println("I have been learning Java by myself, nobody examined me how thorough is my knowledge and how good is my code.");
            } else if (this.experience.equals("getJavaJob")) {
                System.out.println("I passed succesfully getJavaJob exams and code reviews.");
            } else {
                System.out.println("I don't know about my experience, sorry.");
            }
            System.out.println("\n"); // просто что бы отделить реплики
        }
    }

    static class Database {
        private static ArrayList<Candidate> candidatelist = new ArrayList<>();

        Database() {
            initDatabase();
        }

        static void initDatabase() {
            candidatelist.add(new Candidate("Andrew", "self-learner"));
            candidatelist.add(new Candidate("Loyd", "self-learner"));
            candidatelist.add(new Candidate("Peter", "self-learner"));
            candidatelist.add(new Candidate("Max", "self-learner"));
            candidatelist.add(new Candidate("Andrew", "self-learner"));
            candidatelist.add(new Candidate("Nikita", "self-learner"));
            candidatelist.add(new Candidate("Leonid", "self-learner"));
            candidatelist.add(new Candidate("Andrew", "self-learner"));
            candidatelist.add(new Candidate("Peter", "self-learner"));
            candidatelist.add(new Candidate("Alexey", "getJavaJob"));
        }
    }

    public static void main(String[] args) {
        new Database();
        Employer employer = new Employer();
        for (Candidate candidate : Database.candidatelist) {
            employer.hello();
            candidate.hello();
            candidate.desribeExperience();
        }
    }
}
