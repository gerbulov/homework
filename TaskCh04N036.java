import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Сколько прошло времени в минутах?");
        float time = sc.nextFloat();
        if (time % 5 < 3) {
            System.out.println("Горит зеленый");
        } else System.out.println("Горит красный");
    }
}
