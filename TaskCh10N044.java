public class TaskCh10N044 {
    static int digitalroot(int n) {
        int result;
        if (n % 10 == n) return n;
        int sum = n % 10;
        result = sum + digitalroot(n / 10);
        return result;
    }
    static int calculatedigitalroot (int n) {
        if (n/10==0) return n;
        return calculatedigitalroot(digitalroot(n));
    }

    public static void main(String[] args) {
        System.out.println(calculatedigitalroot(37252));
    }
}
