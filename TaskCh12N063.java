import java.util.Arrays;

public class TaskCh12N063 {
    static int[] averagenumber(int[][] array) {
        int[] average = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int quantity = 0;
            for (int j = 0; j < array[i].length; j++) quantity += array[i][j];
            System.out.println(quantity + "/" + array[i].length);
            average[i] = Math.round(quantity / array[i].length);
        }
        return average;
    }

    static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Среднее количество учеников " + (i + 1) + " класса = " + arr[i]);
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
                {20, 21, 22, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {20, 22, 30, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {20, 22, 25, 30},
                {30, 30, 29, 30}
        };
        print(averagenumber(arr));
    }
}
