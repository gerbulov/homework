import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = sc.nextLine();
        StringBuilder wordreverse = new StringBuilder(word);
        System.out.println(wordreverse.reverse());
    }
}
