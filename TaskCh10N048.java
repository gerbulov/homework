public class TaskCh10N048 {
    static int maxnum(int arr[],int aln) {
        if (aln<0) return Integer.MIN_VALUE;
        int result = arr[aln];
        if (result<maxnum(arr, aln-1)) {
            result=maxnum(arr,aln-1);
        }
        return result;
    }

    public static void main(String[] args) {
        int arr[] = {4, 5, 2, 1, 6, 7, 3, 2};
        System.out.println(maxnum(arr, arr.length-1));
    }
}
