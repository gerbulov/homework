import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);

        System.out.println("Введите текущий месяц");
        int m = n.nextInt();
        if (m <= 0 || m > 12) {
            System.out.println("Вы ввели неправильный месяц");
            return;
        }

        System.out.println("Введите текущий год");
        int y = n.nextInt();

        System.out.println("Введите месяц рождения");
        int mb = n.nextInt();
        if (mb <= 0 || mb > 12) {
            System.out.println("Вы ввели неправильный месяц");
            return;
        }

        System.out.println("Введите год рождения");
        int yb = n.nextInt();

        if (y <= yb) {
            System.out.println("Человек еще не родился"); //(*считаем что человек до года это нерожденный человек также по условиям задачи) Поэтому не делаем проверку на месяц)
        }

        int age = y - yb;
        if (mb>m) age--;
        System.out.println("Возраст: " + age);
    }
}
