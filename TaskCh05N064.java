public class TaskCh05N064 {

    public static double arg(double[][] S) {
        double result = 0;
        double[] area = new double[S[0].length];

        for (int i = 0; i < S[0].length; i++) {
            area[i] = S[0][i] / S[1][i];
            System.out.println(S[0][i] + "/" + S[1][i] + "=" + area[i]);
            result += area[i];
        }
        return Math.round(result / 12);
    }

    public static void main(String[] args) {
        double[][] PeoplesAndSquares = {
                {100, 200, 320, 420, 542, 654, 775, 865, 998, 1020, 1102, 1201},
                {20, 24, 27, 28, 10, 12, 25, 53, 23, 12, 23, 23}
        };
        System.out.println(arg(PeoplesAndSquares)+" человек на километр");
    }
}
