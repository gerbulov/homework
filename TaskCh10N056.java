import java.util.Scanner;

public class TaskCh10N056 {
    static int result(int number, int delenie) {
        if (delenie == 1) {
            return 1;
        }
        if (number%delenie==0) {
            return 0;
        } else {
            return result(number, delenie-1);
        }
    }
    public static void main(String[] args) {
        boolean result;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число");
        int n = sc.nextInt();
        int delenie = (int) Math.ceil(Math.sqrt(n)); // берем квадратный корень и округляем его до целого числа
        System.out.println(result(n, delenie));
    }
}
