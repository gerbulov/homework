import java.util.Arrays;

public class TaskCh12N025 {
    static int[][] array1(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array2(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = 0; j < b; j++) {
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array3(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = 0; i < a; i++) {
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array4(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = 0; j < b; j++) {
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array5(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
            i++;
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array6(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = 0; j < b; j++) {
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
            j++;
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array7(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = a - 1; i >= 0; i--) {
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array8(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = b - 1; j >= 0; j--) {
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array9(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = a - 1; i >= 0; i--) {
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array10(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = b - 1; j >= 0; j--) {
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array11(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = a - 1; i >= 0; i--) {
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
            i--;
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array12(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = 0; i < a; i++) {
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
            i++;
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array13(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = b-1; j >= 0; j--) {
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
            j--;
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array14(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = 0; j < b; j++) {
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
            j++;
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array15(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int i = a-1; i >= 0; i--) {
            for (int j = b - 1; j >= 0; j--, count++) {
                arr[i][j] = count;
            }
            i--;
            for (int j = 0; j < b; j++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static int[][] array16(int a, int b) {
        int[][] arr = new int[a][b];
        int count = 1;
        for (int j = b-1; j >= 0; j--) {
            for (int i = a - 1; i >= 0; i--, count++) {
                arr[i][j] = count;
            }
            j--;
            for (int i = 0; i < a; i++, count++) {
                arr[i][j] = count;
            }
        }
        return arr;
    }

    static void print(int[][] arr) {
        System.out.println(Arrays.deepToString(arr));
    }

    public static void main(String[] args) {
        print(array1(12, 10));
        print(array2(12, 10));
        print(array3(12, 10));
        print(array4(12, 10));
        print(array5(10, 12));
        print(array6(12, 10));
        print(array7(12, 10));
        print(array8(12, 10));
        print(array9(12, 10));
        print(array10(12, 10));
        print(array11(12, 10));
        print(array12(12, 10));
        print(array13(12, 10));
        print(array14(12, 10));
        print(array15(12, 10));
        print(array16(12, 10));

    }
}
