public class TaskCh10N053 {
    static void reversearr(int[] arr, int length) {
        if (length > 0) {
            System.out.print(arr[length - 1] + " ");
            reversearr(arr, length - 1);
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 7, 7, 9, 1};
        int length = arr.length;
        reversearr(arr, length);
    }
}
