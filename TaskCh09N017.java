import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = sc.nextLine();
        word = word.toLowerCase();
        if (word.charAt(0) == word.charAt(word.length()-1)) System.out.println("Слово начинается и заканчивается на одну и ту же букву");
        else System.out.println("Слово начинается и заканчивается на разные буквы");
    }
}
