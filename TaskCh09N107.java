import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = sc.nextLine();
        StringBuilder wordreverse = new StringBuilder(word);
        int a = word.indexOf('а');
        int o = wordreverse.reverse().indexOf(String.valueOf('о'));
        System.out.println(o);
        System.out.println(a);
        if (a != -1 & o != -1) {
            wordreverse.reverse();
            wordreverse.setCharAt(a, 'о');
            wordreverse.setCharAt(wordreverse.length() - o - 1, 'а');
            System.out.println(wordreverse);
        } else System.out.println("Строка не содержит символа а или о или двух");
    }
}
