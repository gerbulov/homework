import java.util.Scanner;

public class TaskCh10N052 {

    static void reverse(int n) {
        if (n % 10 != 0) {
            System.out.print(n % 10);
            reverse(n / 10);

        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input number:");
        int n = sc.nextInt();
        System.out.println("Reverse number:");
        reverse(n);
    }
}
