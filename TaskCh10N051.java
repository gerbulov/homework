public class TaskCh10N051 {
    static void function1(int n) {
        if (n > 0) {
            System.out.print(n);
            function1(n - 1);
        }
    }

    static void function2(int n) {
        if (n > 0) {
            function2(n - 1);
            System.out.print(n);
        }
    }

    static void function3(int n) {
        if (n > 0) {
            System.out.print(n);
            function3(n - 1);
            System.out.print(n);
        }
    }

    public static void main(String[] args) {
        System.out.println("f1:");
        function1(5);
        System.out.println("\nf2:");
        function2(5);
        System.out.println("\nf3:");
        function3(5);
    }
}
