import java.util.Arrays;

public class TaskCh12N023 {
    static int[][] array1(int a, int b) {
        int[][] arr = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (i == j || a - 1 == i + j) arr[i][j] = 1;
            }
        }
        return arr;
    }

    static int[][] array2(int a, int b) {
        int[][] arr = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (i == j || a - 1 == i + j || a / 2 == j || b / 2 == i) arr[i][j] = 1;
            }
        }
        return arr;
    }

    static int[][] array3(int a, int b) {
        int[][] arr = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (j >= i && j <= a - 1 - i) arr[i][j] = 1;
            }
        }
        for (int i = a / 2 + 1; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (j <= i && j > a - 2 - i) arr[i][j] = 1;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(array1(7, 7)));
        System.out.println(Arrays.deepToString(array2(7, 7)));
        System.out.println(Arrays.deepToString(array3(7, 7)));
    }
}
