import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        float[] usdtorub = new float[20];
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите текущий курс доллара:");
        float course = sc.nextFloat();
        for (int i = 0; i < 20; i++) {
            usdtorub[i] = (i + 1) * course;
            System.out.println(i + 1 + "$\t| " + usdtorub[i] + "рублей");
        }

    }
}
