public class TaskCh03N029 {
    // Записать условие, которое является истинным, когда:
    // а) каждое из чисел X и Y нечетное;
    public static boolean oddnumbers(int x, int y) {
        if (x % 2 == y % 2) {
            return x % 2 == 1;
        } else return false;

    }

    // б) только одно из чисел X и Y меньше 2; - НЕ УВЕРЕН В ПРАВИЛЬНОСТИ по условиям
    public static boolean oneminnumber(int x, int y) {
        return x < 2 ^ y < 2;
    }

    // в) хотя бы одно из чисел X и Y равно нулю;
    public static boolean onenull(int x, int y) {
        if (x == 0) {
            return true;
        } else return y == 0;
    }

    // г) каждое из чисел X, Y, Z отрицательное;
    public static boolean threenegative(int x, int y, int z) {
        if (x < 0) {
            if (y < 0) {
                return z < 0;
            }
        }
        return false;
    }

    // д) только одно из чисел X, Y и Z кратно пяти; - НЕ УВЕРЕН В ПРАВИЛЬНОСТИ по условиям
    public static boolean onefive(int x, int y, int z) {
        boolean a = x % 5 == 0, b = y % 5 == 0, c = z % 5 == 0;
        return !(a && b && c) && (a ^ b ^ c);
    }

    // е) хотя бы одно из чисел X, Y, Z больше 100
    public static boolean minone(int x, int y, int z) {
        if (x > 100) {
            return true;
        } else if (y > 100) {
            return true;
        } else return z > 100;
    }

    public static void main(String[] args) {

    }
}
