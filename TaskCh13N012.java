import java.util.ArrayList;
import java.util.Calendar;

public class TaskCh13N012 {
    public static void main(String[] args) {
        new base();
        base.searchByHowYear(3);
    }

    static class Worker {
        String lastname, firstname, middlename, adress;
        int monthfrom, yearfrom;

        Worker(String lastname, String firstname, String middlename, String adress, int monthfrom, int yearfrom) {
            setLastName(lastname);
            setFirstName(firstname);
            setMiddleName(middlename);
            setAdress(adress);
            setMonthFrom(monthfrom);
            setYearFrom(yearfrom);
        }

        public void setLastName(String lastname) {
            this.lastname = lastname;
        }

        String getLastName() {
            return lastname;
        }

        public void setFirstName(String firstname) {
            this.firstname = firstname;
        }

        String getFirstName() {
            return firstname;
        }

        public void setMiddleName(String middlename) {
            this.middlename = middlename;
        }

        String getMiddleName() {
            return middlename;
        }

        public void setAdress(String adress) {
            this.adress = adress;
        }

        String getAdress() {
            return adress;
        }

        public void setMonthFrom(int monthfrom) {
            this.monthfrom = monthfrom;
        }

        int getMonthFrom() {
            return monthfrom;
        }

        public void setYearFrom(int yearfrom) {
            this.yearfrom = yearfrom;
        }

        int getYearFrom() {
            return yearfrom;
        }

        int howManyYearWork() {
            int todayYear = 2020;
            int todayMonth = 4;
            if (todayMonth >= this.getMonthFrom()) {
                return todayYear - this.getYearFrom();
            } else {
                return todayYear - this.getYearFrom() - 1;
            }
        }
    }

    static class base {
        private static ArrayList<Worker> peoples = new ArrayList<>();

        base() {
            initbase();
        }

        static void initbase() {
            peoples.add(new Worker("Иванов", "Алексей", "Викторович", "Москва", 1, 2017));
            peoples.add(new Worker("Степнов", "Андрей", "Иванович", "Москва", 2, 2018));
            peoples.add(new Worker("Сидоров", "Дмитрий", "Степаныч", "Химки", 3, 2016));
            peoples.add(new Worker("Благин", "Александр", "Сергеевич", "Владимир", 4, 2014));
            peoples.add(new Worker("Серов", "Алексей", "Викторович", "Москва", 1, 2018));
            peoples.add(new Worker("Ганжупас", "Андрей", "Иванович", "Москва", 2, 2018));
            peoples.add(new Worker("Прокторов", "Дмитрий", "Степаныч", "Химки", 3, 2015));
            peoples.add(new Worker("Саров", "Александр", "Сергеевич", "Владимир", 4, 2015));
            peoples.add(new Worker("Игнатов", "Алексей", "Викторович", "Москва", 1, 2019));
            peoples.add(new Worker("Блабло", "Андрей", "Иванович", "Москва", 2, 2018));
            peoples.add(new Worker("Саку", "Дмитрий", "Степаныч", "Химки", 3, 2019));
            peoples.add(new Worker("Митов", "Александр", "Сергеевич", "Владимир", 4, 2018));
            peoples.add(new Worker("Ратов", "Алексей", "Викторович", "Москва", 1, 2020));
            peoples.add(new Worker("Иринова", "Ирина", "Ивановна", "Москва", 2, 2013));
            peoples.add(new Worker("Мединская", "Елизавета", "Степановна", "Химки", 3, 2013));
            peoples.add(new Worker("Сериуева", "Александра", "Сергеевна", "Владимир", 4, 2018));
            peoples.add(new Worker("Павлова", "Люся", "Викторовна", "Москва", 1, 2014));
            peoples.add(new Worker("Клементьева", "Дуся", "Ивановна", "Москва", 2, 2013));
            peoples.add(new Worker("Марычева", "Мария", "Степановна", "Химки", 3, 2013));
            peoples.add(new Worker("Благина", "Александра", "Сергеевна", "Владимир", 4, 2014));
        }

        public static ArrayList<Worker> searchByHowYear(int years) {
            ArrayList<Worker> workers = new ArrayList<>();
            for (Worker list : base.peoples) {
                if (list.howManyYearWork() >= years) {
                    workers.add(list);
                }
            }
            printworkers(workers, years);
            return workers;
        }

        static void printworkers(ArrayList<Worker> workers, int years) {
            System.out.println("Вот эти ребята работают больше " + years + " лет:");
            for (Worker list : workers) {
                System.out.println(list.getLastName() + " " + list.getFirstName() + " " + list.getMiddleName());
            }
        }
    }
}
